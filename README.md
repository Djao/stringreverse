# Question 10

## Description

<details><summary>Click to expand</summary>
Make a program in C/C++ or Java language that reverses a string informed by the user without using any temporary variable, buffer or any pre-existing function or method for this.
</details>

## Live-Test

In this link [https://onlinegdb.com/qKIQoT-Jy](https://onlinegdb.com/qKIQoT-Jy) it's possible to run the solution online.

## Comments
In order to reverse a string the function first calculates the length like in [https://gitlab.com/Djao/stringlength](https://gitlab.com/Djao/stringlength) counting characters till it finds the '\0' null character. Then it uses the XOR operator in the characters that need to be swapped 3 times in the following way.  
a = a XOR b.  
b = a XOR b.  
a = a XOR b.  
The way this works, if we take a look at the XOR truth table.  
| i | j | output
| ------ | ------ | ------ |
| false | false | false |
| false | true | true |
| true | false | true |
| true | true | false |  

And if we XOR a variable to itself.  
| i | i | output
| ------ | ------ | ------ |
| false | false | false |
| true | true | false |  

When it executes |a = a XOR b| a now stores |a XOR b|.  
Then when it executes |b = a XOR b| it's equivalent to doing |b = a XOR b XOR b| looking at the second truth table |b XOR b| will always be false, so it becomes |b = a XOR false| now looking at the first truth table |false XOR j| will always be j, so |b = a XOR false| is equivalent to |b = a|  
Then it runs the last line |a = a XOR b| it's equivalent to |a = a XOR b XOR a| -> |a = b XOR false| -> |a = b|  

That way we are able to swap 2 variables without using a third temporary variable.
