/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <iostream>
#include <cstring>  

using namespace std;
string Reverse(string s);

int main()
{
    string s = "";
    cin>>s;

    cout<<Reverse(s)<<endl;

    return 0;
}

string Reverse(string s) {
    int l = -1;
    while (s[++l] != '\0');
    for (int i = 0; i < (l/2); i++) {
        s[i] ^= s[l - i - 1];
        s[l - i - 1] ^= s[i];
        s[i] ^= s[l - i - 1];
    }
    return s;
}
